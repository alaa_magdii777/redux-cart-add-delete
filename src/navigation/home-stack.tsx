import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {homeStack} from './config/navigator';
import FoodList from '../screens/home/foodList';
import Form from '../screens/home/form';

const Stack = createStackNavigator();

export default ({name}: any) => {
  return (
    //@ts-ignore
    <Stack.Navigator name={name} headerMode="none">
      <Stack.Screen name={homeStack.form} component={Form} />
      <Stack.Screen name={homeStack.foodList} component={FoodList} /> 
    </Stack.Navigator>
  );
};
