import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {rootSwitch } from './config/navigator';
import homeStack from './home-stack';
const Stack = createStackNavigator();

const AppNavigator = ({name}: any) => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name={rootSwitch.homeStack} component={homeStack} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
