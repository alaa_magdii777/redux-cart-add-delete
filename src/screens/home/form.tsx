import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import * as React from 'react';
import { Text, View, StyleSheet, TextInput, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { connect, useDispatch } from 'react-redux';
import { addFood } from '../../actions/food';
import { homeStack } from '../../navigation/config/navigator';

const Form = () => {
  const [food,setFood]=React.useState('')
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const submitFood = (food:any) => dispatch(addFood(food))
  return (
    <View style={styles.container}>
      <View style={{ alignItems:"center",}}>
      <Image source={require("../../img/add-form.png")} style={{height:300,width:"90%",resizeMode:"contain"}}/>
      <Text style={[styles.txtForm,{marginVertical:30}]}>Add your List Food</Text>
      <TextInput
        style={styles.input}
        onChangeText={setFood}
        value={food}
        placeholder="Enter Food Name"
      />
      </View>
      <TouchableOpacity activeOpacity={.6} style={styles.btn}
      onPress={async()=>{
        submitFood(food);
        const savedArr = await AsyncStorage.getItem('food')
        let _savedArr;
        if (!savedArr) _savedArr = []
        else _savedArr = JSON.parse(savedArr)
        const newArray = [..._savedArr, food]
        // ====
        await AsyncStorage.setItem(
          'food',
          JSON.stringify(newArray)
      );
// ==========
        navigation.navigate(homeStack.foodList);
        
      }} >
       <Text style={[styles.txtForm,{fontSize:16,fontWeight:"bold"}]}>Submit</Text>
      </TouchableOpacity>
    </View>
  );
};

// const mapStateToProps=(state:any)=>{
// return{
//   foods:state.foodReducer.foodList
// }
// }

// const mapDispatchToProps=(dispatch:any)=>{
//   return{
//     add:(food:any)=>dispatch(addFood(food))
//   }
//   }

// export default connect (mapStateToProps,mapDispatchToProps)(Form);
export default Form;

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:"white",
    justifyContent:"center",
  },
  input:{
    width:"90%",
    borderRadius:25,
    borderWidth:1,
    borderColor:"gray",
    paddingHorizontal:20
  },
  txtForm:{color:"gray",fontSize:25},
  btn:{
    backgroundColor:"#92E3A9",
    height:50,
    width:"90%",
    alignSelf:"center",
    marginTop:30,
    borderRadius:25,
    alignItems:"center",
    alignContent:"center",
    justifyContent: 'center',
  }
});
