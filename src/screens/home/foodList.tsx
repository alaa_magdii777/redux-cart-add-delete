import AsyncStorage from '@react-native-async-storage/async-storage';
import * as React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {deleteFood} from '../../actions/food';

const FoodList = () => {

  const dispatch = useDispatch();
  const deleteCurrent = (key: any) => dispatch(deleteFood(key));
  const [newArray, setNewArray] = React.useState([]);
  //@ts-ignore
  const foods = useSelector(state => state.foodReducer.foodList);
  React.useEffect(() => {
    AsyncStorage.getItem("food").then((value: any) => {
      JSON.parse(value)
      setNewArray(value)
    })
  }, []);

  return (
    <View style={styles.container}>
      <Text style={[styles.txt,{fontSize:20,alignSelf:"center",marginTop:50}]}>List Food</Text>
      <FlatList
        style={styles.listContainer}
        data={foods}
        keyExtractor={(item, index) => item.key.toString()}
        renderItem={data => (
          <View style={styles.row}>
            <Text style={styles.txt}>
              {data.item.name}
            </Text>
            <TouchableOpacity
              style={{height:20,width:20}} activeOpacity={0.6}
              onPress={() => {deleteCurrent(data.item.key),console.log(data.item.key,"data.item.key")}}>
              <Image source={require('../../img/trash-green.png')} style={{height:15,width:15,resizeMode:"contain"}}/>
            </TouchableOpacity>
          </View>
        )}
      />
    </View>
  );
};

// const mapStateToProps=(state:any)=>{
//   return{
//     foods:state.foodReducer.foodList
//   }
//   }

//   const mapDispatchToProps=(dispatch:any)=>{
//     return{
//       delete:(key:any)=>dispatch(deleteFood(key))
//     }
//     }

//   export default connect (mapStateToProps,mapDispatchToProps)(FoodList);

export default FoodList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 16,
  },
  listContainer: {
    marginTop:20

  },
  txt: {color: 'black', fontSize: 16},
  row:{
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-between",
    marginTop:10,
    backgroundColor:"#92E3A9",
    padding:10,
    borderRadius:10
  }
});
