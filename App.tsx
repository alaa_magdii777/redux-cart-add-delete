import AsyncStorage from '@react-native-async-storage/async-storage';
import React from 'react';
import { StatusBar} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import AppNavigator from './src/navigation/rootSwitch';
import configureStore from './src/store';

const App = () => {
  const barStyle = 'dark-content';
  const store=configureStore()
  React.useEffect(() => {
    AsyncStorage.getItem("food").then((value: any) => {
      JSON.parse(value)
      console.log(value,"valueeeeeeeeeeee")
    })
  }, []);

  return (
    <SafeAreaProvider style={{ flex: 1 }}>
      <Provider store={store}>
    <StatusBar translucent barStyle={barStyle} backgroundColor={"white"} />
    <AppNavigator/>
    </Provider>
  </SafeAreaProvider>
  );
};

export default App;

